<?php

class Controller_Base extends Base {
    
  
    
      /**
     * Рендерит вьюху
     * 
     * @param string $path Путь к файлу вьюхи
     * @param array $params Параметры
     * @throws Exception
     */
    public static function render($path, $params = array()) {
        

        
        $_SESSION['current_view'] = '/' . Base::$short_views_path . '/' . dirname($path);
        $_SESSION['vars']  = $params;
              
	if (empty($path)) {
	    throw new Exception(_("Cannot load empty view"));
	}
        
        
        if (file_exists(Base::$views_path . $path))
            include Base::$views_path . $path;

    }
    
    /**
     * Функция-событие, вызывающееся непосредственно до загрузки финального представления. В этом классе ничего не делает,
     * последующие классы могут переопределять эту функцию. Она будет вызвана только в том случае, если в конкретном контроллере
     * она определена
     * 
     * @return void
     */
    public static function onBeforeLoad() {
        
    }
    
    /**
     * Функция-событие, вызывающееся после загрузки финального представления. В этом классе ничего не делает,
     * последующие классы могут переопределять эту функцию. Она будет вызвана только в том случае, если в конкретном контроллере
     * она определена
     * 
     * @return void
     */
    public static function onAfterLoad() {
        
    }
   
}