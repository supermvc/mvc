<?php

/**
 * Базовый класс работы с базами данных
 * 
 * @category Database
 * @package Db
 */

class Db extends Base{
  
    public static $connection_data;
    
    public static $connection_id;
    
    public static $query;
    
    public static $query_id;
    
    public static $last_error;
    
    public static $last_error_num;
    
    public static function initValues() {
        
        $db_values = Base::read_config_file('db');
        
        Db::$connection_data['host'] = trim($db_values['host']);
        Db::$connection_data['user'] = trim($db_values['user']);
        Db::$connection_data['pass'] = trim($db_values['pass']);
        Db::$connection_data['dbname'] = trim($db_values['dbname']);
        Db::$connection_data['port'] = trim($db_values['port']);
   }
     
     public static function wasError() {
         return (empty(Db::$last_error)) ? true : false;
     }
     
     
         
         
     }
   

?>