<?php

/**
 * Класс для работы с MySQL
 * 
 * @category Database
 * @package DB_MySQL
 */

class DB_MySQL extends Db {

/**
 * Проверяет, существует ли база данных. В случае если указан пустой параметр, возвращает исключение. В противном случае
 * вернет true в случае существования базы, и false в противном случае.
 * 
 * @param string $dbname Проверяемая база данных
 * @return mixed
 * @throws Exception
 */    
public static function dbExists($dbname) {
    
    if (empty($dbname)) 
        throw new Exception(_("Cannot operate on an empty value"));
    
    DB_MySQL::query('SHOW DATABASES LIKE "' . $dbname . '"');
    $rows = DB_MySQL::getRowCount();
    return ($rows > 0) ? true : false;
}  

/**
 * Выбирает базу данных для работы. Если база данных, которую пытаются выбрать для работы, не существует на сервере, функция возвращается исключение.
 * В противном случае возвращается результат выборки базы данных.
 * 
 * @param string $dbname База данных, которую необходимо выбрать для работы с ней
 * @return mixed
 * @throws Exception
 */
public static function selectDB($dbname = '') {
    
    if (empty($dbname))
        $dbname = Db::$connection_data['dbname'];
    
    if (!DB_MySQL::dbExists($dbname))
        throw new Exception(sprintf(_("Database %s does not exist"), $dbname));
    
    return mysql_select_db($dbname);
    
}

/**
 * Получает все таблицы из заданной базы данных. Если базы данных, указанной в параметре, не существует, возвращается исключение. 
 * 
 * @param string $dbname База данных, из которой необходимо выбрать все таблицы
 * @return array
 * @throws Exception
 */
public static function getTables($dbname = '') {   
    if(empty($dbname))
        $dbname = Db::$connection_data['dbname'];
    
    if (!DB_MySQL::dbExists($dbname))
        throw new Exception(sprintf(_("Database %s does not exist"), $dbname));
    
    $str = 'SHOW TABLES FROM ' . $dbname;
    $q = DB_MySQL::query($str);
    $var = 'Tables_in_'.$dbname;
    $tables = array();
    
    
    foreach (DB_MySQL::fetch() as $value)
        $tables[] = $value;
    
    
    return $tables;
    
}

/**
 * Проверяет, существует ли таблица в данной базе данных. Возвращает true в случае существования таблицы, false в противном случае.
 * 
 * @param string $tablename Таблица, существование которой необходимо проверить
 * @param string $dbname База данных, в которой должна находиться искомая таблица
 * @return boolean
 */
public static function tableExists($tablename, $dbname = '') {
    
    $tablename = trim($tablename);
    $tables = DB_MySQL::getTables();
    
    return in_array($tablename, $tables);
}

/**
 * Функция выполняет запрос SELECT к базе данных. 
 * 
 * @param string $table Таблица, из которой брать данные. Если такой таблицы нет, возвращается исключение
 * @param mixed $fields Требуемые поля. Если передана пустая строка, будут браться все поля, в противном случае должен быть массив полей. Если это не так, будет передано исключение
 * @param mixed $where Условие для отбора. Если не пустая строка, то массив вида array('key' => 'value')
 * @param mixed $and Логика соединения AND или OR
 * @throws Exception
 * @return mixed
 */
public static function select($table, $fields = '', $where = '', $and = true) {
    
    if (!DB_MySQL::tableExists($table))
        throw new Exception(sprintf(_("Table %s does not exist"), $table));
    
    if (empty($fields)) {
        $fields = '*';
        $selQuery = 'SELECT ' . $fields . ' FROM ' . $table;
    }
    
    else {
        if (!is_array($fields))
            throw new Exception(_("Fields must be an array"));
        
        $str = implode(", ", $fields);
        $selQuery = 'SELECT ' . $str . ' FROM ' . $table;
    }
    
    if (!empty($where)) {
        
        $selQuery .= ' WHERE ';
        $size = count($where);
        
        $i = -1;
        foreach ($where as $key => $value) {
            $i++;
            $selQuery .= $key . ' = ' . $value;       
            
            if ($and)
                $selQuery .= ' AND ';
            else
                $selQuery .= ' OR ';
        }
        
        if ($and)
            $selQuery = substr($selQuery, 0, -5);
        else
            $selQuery = substr($selQuery, 0, -4);
    }
    
    //return $selQuery;
    return DB_MySQL::query($selQuery);
    
}

/**
 * Удаляет таблицу из базы данных. Если такой таблицы не существует, возвращается исключение. 
 * Функция возвращает результат работы функции Db::wasError(), которая определяет, была ли ошибка при выполнении последнего запроса
 * 
 * @param string $table Таблица, подлежащая удалению
 * @return boolean
 * @throws Exception
 */
public static function delete($table) {
    if (!DB_MySQL::tableExists($table))
        throw new Exception(sprintf(_("Table %s does not exist"), $table));
    
    DB_MySQL::query("DROP TABLE ". $table);
    return Db::wasError();
}

/**
 * Функция-обёртка. Возвращает кол-во затронутых строк в результате запроса к БД.
 * 
 * @return integer
 */
public static function getRowCount() {
    
    return mysql_num_rows(Db::$query_id);
}

/**
 * Осуществляет связь с сервером баз данных MySQL. Если связь не удалась, возвращается исключение. В противном случае возвращается
 * ресурсный объект, предназначенный для дальнейшей работы.
 * @return type
 * @throws Exception
 */
public static function connect() {
         Db::initValues();
         Db::$connection_id = mysql_connect(Db::$connection_data['host'], Db::$connection_data['user'], Db::$connection_data['pass']);
         if (!is_resource(Db::$connection_id))
              throw new Exception(_("Cannot connect to MySQL"));
          else {
              mysql_select_db(Db::$connection_data['dbname']);
              return Db::$connection_id;
          }
          
}

/**
 * Функция выполняет запрос к серверу MySQL. Если передан пустой текст запроса, то он берется из переменной $query класса. 
 * В случае успешного запроса возвращается его ID или true/false, в зависимости от типа запроса. Если запрос не удался, возвращается false, а в 
 * соответствующих переменных будет доступен код и текст последней ошибки.
 * 
 * @param string $text Текст запроса
 * @return mixed
 */
public static function query($text) {
    if (empty($text)) {
        $text = Db::$query;
    }
    
    $text = trim($text);
    Db::$query_id = mysql_query($text);
    Db::$last_error = mysql_error();
    Db::$last_error_num = mysql_errno();
    
    if (empty(Db::$last_error))
        return Db::$query_id;
    else
        return false;
}

/**
 * Функция-обертка. Возвращает результаты mysql_fetch_array. В случае неуспеха возвращается исключение.
 * 
 * @return mixed
 * @throws Exception
 */
public static function fetch() {
    if (!is_resource(Db::$query_id))
        throw new Exception(_("Cannot analyze non-resource variable"));
    
    $cnt = DB_MySQL::getRowCount();
    
    for ($i = 0; $i < $cnt; $i++) {    
        $dbresult = mysql_result(Db::$query_id, $i);
        yield $dbresult;
    }

}

/**
 * Возвращает подробную информацию о каждом поле запроса.
 * 
 * @return object
 * @throws Exception
 */
public static function fetchFields() {
   
    if (!is_resource(Db::$query_id))
        throw new Exception(_("Cannot fetch fields on this request"));
    
    $fields_num = mysql_num_fields(Db::$query_id);
    //$result_array = array();
    //$i = 0;
    
    for ($i = 0; $i < $fields_num; $i++) {
        
        $field = mysql_fetch_field(Db::$query_id, $i);
        yield $field;
    }
    
  
   
}

/**
 * Функция ищет первичный ключ в таблице $table. В случае успеха возвращает имя поля первичного ключа. В противном случае, или если первичного ключа
 * нет, возвращает false
 * 
 * @param string $table Таблица, в которой производим поиск
 * @return mixed
 * @throws Exception
 */
public static function findPK($table) {
    
    if (!DB_MySQL::tableExists($table))
        throw new Exception(_("Table does not exist"));
    
    DB_MySQL::select($table);
    $fields = DB_MySQL::fetchFields();
    $field_count = count($fields);
    
    for ($i = 0; $i <= $field_count; $i++) {
        if ($fields[$i]['primary_key'] == 1) {
            return $fields[$i]['name'];
            break;
        }
    }
    
    return false;
}

/**
 * Ищет запись в таблице по значению первичного ключа. Если указан параметр $returnValue, функция возвратит элемент массива с этим ключом, иначе говоря - 
 * определенное поле. В противном случае возвратится вся строка.
 * 
 * @param string $table Таблица, которая участвует в поиске
 * @param integer $pkValue Значение первичного ключа
 * @param string $returnValue Ключ элемента массива, который необходимо вернуть
 * 
 * @return mixed
 */
public static function findRowByPK($table, $pkValue, $returnValue = '') {
    
   $pk = DB_MySQL::findPK($table);
   DB_MySQL::select($table, '', array($pk => $pkValue));
   
   $result = DB_MySQL::fetch();
   
   if (!empty($returnValue)) {
       
       if (array_key_exists($returnValue, $result))
               return $result[$returnValue];
       else
                return $result;
   }
   
   else {
       return $result;
   }
   
}

/**
 * Функция обновляет заданные значения в заданной таблице
 * 
 * @param string $table Таблица, значения полей которой должны быть обновлены
 * @param array $fields_to_update Массив пар "ключ-значение". Ключ - имя поля, значение - новое значение поля
 * @param array $fields_where Массив пар "ключ значение" полей в условии where.
 * @param type $and Логика обновления. По умолчанию true, значит условия объединяет логическое "И". В противном случае - "ИЛИ"
 * 
 * @return resource
 * @throws Exception
 */
public static function update($table, array $fields_to_update, array $new_values, array $fields_where, $and = true) {
    
    if (!DB_MySQL::tableExists($table))
        throw new Exception(sprintf(_("Table %s does not exist"), $table));
    
    if (empty($fields_to_update))
        throw new Exception(_("No fields to update"));
    
    $fieldsCnt = count($fields_to_update);
    $valuesCnt = count($new_values);
       
    /**
     * Реализовать проверку на существование данных, указанных в $fields_where
     */
    if (!empty($where)) {
        foreach ($fields_where as $key => $value) {
            
        }
    }
    
    if ($fieldsCnt !== $valuesCnt)
        throw new Exception(_("Arrays have to be identical"));
    
    array_walk($new_values, function(&$new_values) { if (!is_int($new_values)) $new_values = chr(34).$new_values . chr(34);});
    
    $updQuery = "UPDATE ".$table . " SET ";
    
    for ($i = 0; $i < $fieldsCnt; $i++) {
        $upd[] = $fields_to_update[$i] . " = " . $new_values[$i];
    }
    
    $updQuery .= implode(", ", $upd);
    
    if (!empty($fields_where)) {
        
        $updQuery .= ' WHERE ';
        foreach ($fields_where as $key=>$value) {
            $updQuery .= $key.' = "'.$value.'"';
             if ($and)
                $updQuery .= ' AND ';
            else 
                $updQuery .= ' OR ';     
        }
        
         if ($and)
            $updQuery = substr($updQuery, 0, -5);
        else
            $updQuery = substr($updQuery, 0, -4);
    }
    
    
    DB_MySQL::query($updQuery);
    DB::$last_error = mysql_error();
    
    if (empty(DB::$last_error))
        return true;
    else
        return false;
    
    
}


public static function insert($table, array $cols, array $values) {
    
    if (!DB_MySQL::tableExists($table))
        throw new Exception(_("The table where the insert operation supposed to occur, does not exist"));
    
    
    array_walk($values, function(&$values) { if (!is_int($values)) $values = chr(34).$values . chr(34);});
    
    
    $colsString = implode(", ", $cols);
    $valuesString = implode(", ", $values);
    
    $insertQuery = "INSERT INTO " . $table . "(" . $colsString . ") VALUES (".$valuesString.")";
    
   
    $q = DB_MySQL::query($insertQuery);
    
    return Db::wasError();
    
}

}

?>