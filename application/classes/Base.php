<?php

class Base {
       
    /**
     * Переменная, в которой хранится путь к классам системы
     * 
     * @var string
     * @access public
     * @static
     */
    public static $class_path;
    
    /**
     * Расширение php-файла. Можно поменять, к примеру, на .phtml или любое другое
     * 
     * @var string
     * @access public
     * @static
     */
    public static $php_ext;
    
    /**
     * В этой переменной хранится путь к контроллерам системы
     * 
     * @var string
     * @access public
     * @static
     */
    public static $controllers_path;
    
    /**
     * Путь к вьюхам
     * 
     * @var string
     * @access public
     * @static
     */
    public static $views_path;
    
    /**
     * Путь к моделям
     * 
     * @var string
     * @access public
     * @static
     */
    public static $models_path;
    
    /**
     * @ignore
     */
    public static $short_views_path;
    
    /**
     * Путь к конфигурационным файлам
     * 
     * @var string
     * @access public
     * @static
     */
    public static $config_path;
    
    /**
     * Путь к файлам CSS
     * 
     * @var string
     * @access public
     * @static
     */    
    public static $css_path;
    
    /**
     * Путь к картинкам
     * 
     * @var string
     * @access public
     * @static
     */
    public static $img_path;
    
    /**
     * Используемый слой абстракции баз данных
     * 
     * @var string
     */
    public static $db_layer;
    
    /**
     * Инициализационная функция базового класса. Исходим из предположения, что DOCUMENT_ROOT указывает на public!
     * 
     * @access public
     * @return void
     */
    public static function init() {
         $root = '../application/';
         $views_root = '../public/';
        
        Base::$class_path = $root . 'classes/';
        Base::$controllers_path = $root . 'controllers/';
        Base::$php_ext = '.php';
        Base::$views_path = $views_root . 'views/';
        Base::$config_path = $root . 'config/';
        Base::$short_views_path = 'views';
        Base::$css_path = '/css/';
        Base::$img_path = '/img/';
        Base::$models_path = $root . 'models/';
        
        putenv ("LC_ALL=ru_RU.utf-8");
        setlocale (LC_ALL, "ru_RU.utf-8");
        $domain = 'messages';
        
        bind_textdomain_codeset($domain, 'UTF-8');
        
        bindtextdomain ($domain, "../application/locale");
        
        textdomain ($domain);
        
        
        
        if (version_compare(PHP_VERSION, '5.5.0', '<')) {
                          
               throw new Exception(sprintf(_("You have to install PHP version not lower than 5.5.0. The version, that is currently being run on your server is %s", PHP_VERSION)));
        
        }
        
        $mysql_exists = function_exists('mysql_connect') ? true: false;
        $mysqli_exists = function_exists('mysqli_connect') ? true: false;
        $pdo_mysql_exists = class_exists('PDO') ? true : false;
        
        if (!$mysql_exists and !$mysqli_exists and !$pdo_mysql_exists) {
            
            throw new Exception(_("The functionality of this CMS relies upon database. We couldn't detect any on your server."
                    . "In order to continue you have to either install or enable one of the following: mysql, mysqli, pdo_mysql"));
        }
        
        if (function_exists('mysql_connect')) {
            Base::$db_layer = 'MySQL';
        }
        
        
        
        
    }
    
     
    /**
     * Чтение конфигурационных файлов
     * 
     * @param string $config
     * @return array
     * @throws Exception
     */
    public static function read_config_file($config) {
        
        $path = Base::$config_path . $config . '.ini';
        if (!file_exists($path))
            throw new Exception(sprintf(_("Chosen configuration file %s does not exist"), $config.'.ini'));
        
        $parse = parse_ini_file($path);
        return $parse;
    }
    
  
    
    /**
     * Автозагрузчик классов
     * 
     * @param string $class Класс к загрузке
     * @throws Exception
     */
    public static function load_classes($class) {
    
        
    if (!file_exists(Base::$class_path . '/' . $class . Base::$php_ext) and
        !file_exists(Base::$controllers_path . '/' . $class . Base::$php_ext) and
        !file_exists(Base::$models_path . '/' . $class . Base::$php_ext))
            throw new Exception(sprintf(_("Cannot load class %s"), $class));
    
        if (file_exists(Base::$class_path .'/' .  $class . Base::$php_ext))
        	require_once(Base::$class_path. '/' .  $class . Base::$php_ext);
        
    
        if (file_exists(Base::$controllers_path .'/' .  $class . Base::$php_ext))
	require_once(Base::$controllers_path. '/' .  $class . Base::$php_ext);
        
  
    
    
       if (file_exists(Base::$models_path .'/' .  $class . Base::$php_ext))
	require_once(Base::$models_path. '/' .  $class . Base::$php_ext);
}


}
?>