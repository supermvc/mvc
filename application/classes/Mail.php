<?php

/**
 * Класс отправки почты. Работает как посредством mail() так и через smtp
 *
 * @author mkreine
 */
class Mail extends Base {
    
    /**
     * Кому отправляем письмо
     * 
     * @var string
     */
    public static $to;
    
    
    /**
     * Должно ли быть письмо в html? true - да, false - нет
     * 
     * @var boolean
     */
    public static $html;
    
    /**
     * Тема письма
     * 
     * @var string
     */
    public static $subject;
    
    /**
     * От кого письмо
     * 
     * @var string
     */
    public static $from;
    
    /**
     * Кому отвечаем
     * 
     * @var string
     */
    public static $replyto;
    
    /**
     * Само письмо
     * 
     * @var string
     */
    public static $message;
    
    /**
     * Копия письма
     * 
     * @var string
     */
    public static $cc;
    
    /**
     * Слепая копия письма
     * 
     * @var string
     */
    public static $bcc;
    
    public static function initValues() {
        
        $p = Base::read_config_file('mail');
        
        self::$from     =   trim($p['from']);
        self::$replyto  =   trim($p['replyto']);        
    }
    
    public static function setHTMLMode($bool) {
        
        self::$html     =   $bool;
    }
    
    public static function setTo($email) {
        
        self::$to   =   trim($email);
    }
    
    public static function setSubject($subject) {
        
        self::$subject  =   trim($subject);
    }
    
    public static function setMessage($msg) {
        
        self::$message  =   trim($msg);
    }
    
    public static function formHeaders() {
        
        $headers = array();
        
        $headers[] = "From: " . self::$from."\r\n";
        if (self::$html) {
            
            $headers[] = "MIME-Version: 1.0\r\n";
            $headers[] = "Content-Type: text/html; charset=utf-8\r\n";
        }
        
        if (!empty(self::$cc))
            $headers[] = "Cc: ".self::$cc;
        
        if (!empty(self::$bcc))
            $headers[] = "Bcc: ".self::$bcc;
        
        return $headers;
    }
    
    public static function send() {
       
        echo chr(34).self::$to.chr(34);
        
     
        if (!empty(self::$to))
            if (mail(chr(34).self::$to.chr(34), chr(34).self::$subject.chr(34), chr(34).self::$message.chr(34), chr(34).implode('', self::formHeaders()).chr(34)))
                echo "Письмо отправлено";
             else
                echo "Письмо не отправлено";
        
    }
    
}
