<?php

class Router extends Base {
    
    public static function formURL() {
        $script = $_SERVER['SCRIPT_NAME'];
        $query = $_SERVER['QUERY_STRING'];
        $full_script = $script . '?' . $query;
        $full_script = ltrim($full_script, '/');
        
        return $full_script;
    }

    public static function route($url) {
	if (empty($url)) {
	    return false;
	}
	
	$controller = isset($url['controller']) ? $url['controller'] . 'Controller' : 'indexController';
	$action = isset($url['action']) ? $url['action'] . 'Action' : 'indexAction';
        
            
        if (!method_exists($controller, $action))  {
            trigger_error(sprintf(_("There is no method %s in class %s"), $action, $controller), E_USER_ERROR);
        }
        
	/*if (empty($controller) || empty($action)) {
	    throw new Exception("Malformed url!");
	}*/

	if (file_exists(Base::$controllers_path . $controller  . Base::$php_ext)) {
            
	    require_once (Base::$controllers_path . $controller  . Base::$php_ext);
	    
	    unset($url['controller']);
	    unset($url['action']);

	    $keys = array_keys($url);

	    if (count($keys) > 0) {
		$params = array();
		foreach ($keys as $key) {
		    if (array_key_exists($key, $url)) {
			$params[$key] = $url[$key];
		    }
		}
	    }
	    
	    if (method_exists($controller, $action)) {
		if (!empty($params)) {
                    if (method_exists($controller, 'onBeforeLoad'))
                            $controller::onBeforeLoad();
                    call_user_func_array("$controller::$action", $params);
                }
			
		else {
                    if (method_exists($controller, 'onBeforeLoad'))
                        $controller::onBeforeLoad();
                    call_user_func("$controller::$action");
                }
                
                if (method_exists($controller, 'onAfterLoad'))
                        $controller::onAfterLoad();
			
	    }
	   
    }

    }
    
}