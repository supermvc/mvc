<?php

class DB_PDO_MySQL extends DB {
    
    public static $pdo;
    
    public static $query_result;
    
    public static function connect() {
        
        $string = 'mysql:host='.Db::$connection_data['host'].';port='.Db::$connection_data['port'].';dbname='.Db::$connection_data['dbname'];
        
        DB_PDO_MySQL::$pdo = new PDO($string, Db::$connection_data['user'], Db::$connection_data['pass'], array(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT));
        
                
        return DB_PDO_MySQL::$pdo;
        
    }
    
    public static function query($text) {
        if (empty($text)) {
            $text = Db::$query;
        }
        
        if (!is_object(DB_PDO_MySQL::$pdo))
            throw new Exception(_("PDO connect was unsuccessful"));
        
        self::$query_result = DB_PDO_MySQL::$pdo->query($text);
        
        Db::$last_error = DB_PDO_MySQL::$pdo->errorInfo();
        Db::$last_error_num = DB_PDO_MySQL::$pdo->errorCode();
        
        return DB_PDO_MySQL::$query_result;
    }
    
    public static function getRowCount() {
    
    $all = DB_PDO_MySQL::$query_result->fetchAll();
    return count($all);
}


    public static function fetch() {
       
    if (!is_object(DB_PDO_MySQL::$query_result))
        throw new Exception(_("Cannot analyze non-object variable")); 
    
    while ($row = DB_PDO_MySQL::$query_result->fetch(PDO::FETCH_NUM))
            yield $row;
  
}

public static function dbExists($dbname) {
    
    if (empty($dbname)) 
        throw new Exception(_("Cannot operate on an empty value"));
    
    DB_PDO_MySQL::query('SHOW DATABASES LIKE "' . $dbname . '"');
    $rows = DB_PDO_MySQL::getRowCount();
    return ($rows > 0) ? true : false;
}  


public static function getTables($dbname = '') {   
    if(empty($dbname))
        $dbname = Db::$connection_data['dbname'];
    
    /*if (!DB_PDO_MySQL::dbExists($dbname))
        trigger_error(sprintf(_("Database %s does not exist"), $dbname), E_USER_WARNING);*/
    
    try {
        DB_PDO_MySQL::dbExists($dbname);
    } catch (Exception $ex) {
        trigger_error(sprintf(_("Database %s does not exist"), $dbname), E_USER_WARNING);
    }
    
    $str = 'SHOW TABLES FROM ' . $dbname;
    DB_PDO_MySQL::query($str);

    $var = 'Tables_in_'.$dbname;
    $tables = DB_PDO_MySQL::fetch();
    
    foreach ($tables as $value => $key)
        $tbl[] = $key[0];
        
  return $tbl;
    
}


public static function insert($table, array $cols, array $values) {
    
    if (!DB_PDO_MySQL::tableExists($table))
        throw new Exception(_("The table where insert operation supposed to occur does not exist"));
    
    
    array_walk($values, function(&$values) { if (!is_int($values)) $values = chr(34).$values . chr(34);});
    
    
    /* Кол-во значений должно быть равно кол-ву полей. Функция не поддерживает работу с многомерными массивами */
    $colCount = count($cols);
    $valCount = count($values);
    
    
    //если это не так, бросаем исключение и выходим
    if ($colCount !== $valCount)
        throw new Exception(_("Arrays have to be identical"));
    
    //делаем из массива полей строку
    $colsString = implode(", ", $cols);
    
    $vl = array();
    
    //и из массива значений тоже
    for ($i = 0; $i < $colCount; $i++) {
        $vl[$i] = ':val'.$i;
    }
            
    $vl_string = implode(", ", $vl);
    
    //начинаем запрос
    $r = DB_PDO_MySQL::$pdo->prepare("INSERT INTO " . $table . "(" . $colsString . ") VALUES (".$vl_string.")");
    
    for ($i = 0; $i < $colCount; $i++) {
        
        //и каждому параметру ставим в соответствие реальное значение
        $r->bindParam($vl[$i], $values[$i]);
        
    }
    
    //выполняем
    $r->execute();
    
    
    
}


/**
 * Проверяет, существует ли таблица в данной базе данных. Возвращает true в случае существования таблицы, false в противном случае.
 * 
 * @param string $tablename Таблица, существование которой необходимо проверить
 * @param string $dbname База данных, в которой должна находиться искомая таблица
 * @return boolean
 */
public static function tableExists($tablename, $dbname = '') {
    
    $tablename = trim($tablename);
    $tables = DB_PDO_MySQL::getTables();
    
    return in_array($tablename, $tables);
}



/**
 * Функция обновляет заданные значения в заданной таблице
 * 
 * @param string $table Таблица, значения полей которой должны быть обновлены
 * @param array $fields_to_update Массив полей, которые нужно обновить
 * @param array $new_values Новые значения в полях
 * @param array $fields_where Массив пар "ключ значение" полей в условии where.
 * @param type $and Логика обновления. По умолчанию true, значит условия объединяет логическое "И". В противном случае - "ИЛИ"
 * 
 * @return resource
 * @throws Exception
 */
public static function update($table, array $fields_to_update, array $new_values, array $fields_where, $and = true) {
    
    if (!DB_PDO_MySQL::tableExists($table)) {
        throw new Exception(sprintf(_("Table %s does not exist"), $table));
    }  
    
    if (empty($fields_to_update))
        throw new Exception(_("No fields to update"));
    
    $updQuery = 'UPDATE '.$table . ' SET ';
    
    $cntKeys = count($fields_to_update);
    $cntValues = count($new_values);
    
    if ($cntValues !== $cntKeys)
        throw new Exception(_("Arrays have to be identical"));
    
    
    foreach ($fields_to_update as $key)        
        $upd[] = $key . " = ? "; 
    
    $updQuery .= implode(", ", $upd);
    

    
    
    array_walk($new_values, function(&$new_values) { if (!is_int($new_values)) $new_values = chr(34).$new_values . chr(34);});    
        
    
  
    if (!empty($fields_where)) {
        
        $updQuery .= ' WHERE ';
        foreach ($fields_where as $key=>$value) {
            $updQuery .= $key.' = "'.$value.'"';
             if ($and)
                $updQuery .= ' AND ';
            else 
                $updQuery .= ' OR ';     
        }
        
         if ($and)
            $updQuery = substr($updQuery, 0, -5);
        else
            $updQuery = substr($updQuery, 0, -4);
    }
            echo $updQuery;
            
    
    $prep = DB_PDO_MySQL::$pdo->prepare($updQuery);
    for ($i = 0; $i < $cntKeys; $i++) {
        $prep->bindParam(($i+1), $new_values[$i]);       
    }
    
    $res = $prep->execute($new_values);
    
    
}
    
    
}
   
