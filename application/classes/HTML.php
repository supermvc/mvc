<?php

/**
 * Класс предназначен для внешнего вывода. Методы класса дают возможность выводить различные HTML-элементы, не думая о том,
 * как правильно написать код для их вывода.
 * 
 * @package HTML
 */
class HTML {
    
    public static $doctypes = array(
        'xhtml1.1' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
        'xhtml1.0.frameset' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
        'xhtml1.0.transitional' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
        'xhtml1.0.strict' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
        'html4.01.frameset' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">',
        'html4.01.transitional' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
        'html4.01.strict' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
        'html5' => '<!DOCTYPE html>'
    );
    
    
    public static $targets = array("_blank", "_self", "_parent", "_top");
    
    public static $cache_control = array("public", "private", "no-cache", "no-store");
    
    public static $document_state = array("Static", "Dynamic");
    
    public static $charsets = array("utf-8", "iso-8859-1", "Windows-1251", "Windows-1252", "Windows-1250", "cp866", "koi8-r");
    
    
    /**
     * Выводит текстовое поле.
     * 
     * @param string $name Имя поля. Выводится, если не пусто.
     * @param string $type Если не пусто, то текст, любое другое значение - password
     * @param integer $size Ширина поля. Выводится, если не пусто.
     * @param integer $maxlength Максимальное кол-во символов в поле. Выводится если не пусто.
     * 
     * @return void
     */
    public static function input_text($name = '', $type = '', $size = 0, $maxlength = 0, $value = '') {
        
        if (!empty($type))
            $return = "<input type=\"text\"";
        else 
            $return = "<input type=\"password\"";
        
        
        if (!empty($name))
            $return .= " name=\"$name\"";
        
        if (!empty($size))
            $return .= " size=$size";
        
        if (!empty($maxlength))
            $return .= " maxlength=$maxlength";
        
        if (!empty($value))
            $return .= " value=\"$value\"";
        
        $return .= " />";
        
        echo $return;
    }
    
    /**
     * Выводит кнопку выбора файла для последующей его загрузки на сервер.
     * 
     * @param string $name Если не пусто, то имя поля
     * 
     * @return void
     */
    public static function input_file($name = '') {
        $return = "<input type=\"file\"";
        
        if (!empty($name))
            $return .= " name=\"$name\"";
        
        $return .= " />";
        echo $return;
    }
    
    /**
     * Выводит начальный тег формы.
     * 
     * @param string $action Если не пусто - экшн формы
     * @param string $method Если не пусто - метод отправки данных формы
     * @param string $enctype Enctype - нужно, если отправляются файлы на сервер. Используется, если не пусто
     * @param string $name Имя формы
     * @param string $target Обработчик формы возвращает данные в виде HTML-документа. Этот параметр определяет, куда будет загружен итоговый документ
     * 
     * @return void
     */
    public static function begin_form($action = '', $method = '', $enctype = '', $name = '', $target = '') {
        
        $form = "<form";
        
        if (!empty($name))
            $form .= " name = \"$name\" ";
        
        if (!empty($action))
            $form .= "action = \"$action\" ";
        
        if (!empty($enctype))
            $form .= "enctype=\"$enctype\" ";
        
        if (!empty($method))
            $form .= "method=\"$method\" ";
        
        if (!empty($target))
            $form .= "target = \"$target\"";
        
        $form .= ">";
        
        echo $form;
       
    }
    
    /**
     * Включает в документ текстовую область
     * 
     * @param integer $cols высота области
     * @param integer $rows ширина области
     * @param string $name имя области
     * 
     * @return void 
     */
    public static function input_textarea($cols = 0, $rows = 0, $name = '') {
      
      $return = "<textarea";
      
      if (!empty($cols))
          $return .= " cols=$cols ";
      
      if (!empty($rows))
          $return .= " rows=$rows ";
      
      if (!empty($name))
          $return .= " name=$name ";
      
      echo $return;
      
  }
  
  /**
   * Включает в документ внешний CSS
   * 
   * @param string $file CSS-файл
   * @throws Exception
   * @return void
   */
  public static function include_css($file) {
      //echo $file;
      $ext = explode(".", $file);
     
      
      if ($ext[1] !== 'css')
          throw new Exception("Это не CSS файл");
        
      echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$file\">\r\n";
  }
  
  public static function img($file, $alt = '', $title = '') {
      
      $file = $_SESSION['current_view'] . '/' . $file;
      $ret = "<img src=$file";
      
      if (!empty($alt))
          $ret .= " alt=\"$alt\" ";
      
      if (!empty($title))
          $ret .= " title=\"$title\" ";
      
      $ret .= " />";
      
      echo $ret;
  }
  
  /**
   * Устанавливает заголовок HTML-документа
   * 
   * @param string $title
   * @return void
   */
  public static function set_title($title) {
      
      if (!empty($title))
          echo "<title>$title</title>\r\n";
  }
  
  /**
   * Устанавливает описание в meta
   * 
   * @param string $description
   * @return void
   */
  public static function set_description($description) {
      
      if (!empty($description))
          echo "<meta name=\"description\" content=\"$description\" />\r\n";
  }
  
  /**
   * Устанавливает ключевые слова в meta
   * 
   * @param array $keywords
   * @return void
   */
  public static function set_keywords(array $keywords) {
      
      if (!empty($keywords)) {
          
          $keywords_string = implode(", ", $keywords);
          echo "<meta name=\"keywords\" content=\"$keywords_string \" />\r\n";
      }
  }
  
  /**
   * Включает в документ внешний JS-файл
   * 
   * @param string $file Файл с JS
   * @throws Exception
   * @return void
   */
  public static function include_js($file) {
      
      $ext = substr($file, 0, -2);
      if ($ext !== 'js')
          throw new Exception("Файл не является скриптом JavaScript");
      
      echo "<script language=\"JavaScript\" src=\"$file\"></script>";
  }
  
  public static function set_doctype() {
      echo HTML::$doctypes['html5'];
  }
  
  public static function begin_header() {
      echo "<html>\r\n<head>\r\n";
  }
  
  public static function end_header() {
      echo "</head>";
  }
  
  /**
   * Создаёт блок текста, окруженный тегом <p>. Если $p_class не пустой, то к параграфу применяется класс $p_class 
   * 
   * @param string $text Текст в параграфе
   * @param string $p_class Класс, применяемый к параграфу
   * 
   * @return void
   */
  public static function create_text($text, $p_class = '') {
      
      if (!empty($p_class))
        echo "<p class=\"$p_class\">".$text."</p>";
      else
        echo "<p>".$text."</p>";
  }
  
  /**
   * Создаёт ссылку
   * 
   * @param string $href Адрес URL куда ведет ссылка
   * @param string $link_name Имя ссылки - то, что увидит пользователь
   * @param string $title Тайтл ссылки
   * @param string $class Класс ссылки
   * @param string $target В каком окне откроется адрес ссылки
   * 
   * @return void
   */
  public static function create_link($href, $link_name, $title = '', $class = '', $target = '') {
      
      if (empty($href) || empty($link_name))
          throw new Exception("Недостаточно исходных данных");
      
      $a = "<a href=\"$href\"";
      
      if (!empty($title))
          $a .= " title=\"$title\"";
      
      if (!empty($class))
          $a .= " class=\"$class\"";
      
      if (!empty($target) and in_array($target, HTML::$targets))
          $a .= " target=\"$target\"";
      
      $a .= ">".$link_name."</a>";
      
      echo $a;
  }
  
  public static function no_cache_doc($cache) {
      
      if (!empty($cache) and in_array($cache, HTML::$cache_control))
        echo $return = "<meta http-equiv=\"Cache-control\" content=\"$cache\" />\r\n";
      else
          echo '';
  }
  
  public static function document_state($docstate) {
      
      if (!empty($docstate) and in_array($docstate, HTML::$document_state))
              echo "<meta name=\"document-state\" content=\"$docstate\" />\r\n";
      else
          echo '';
  }
  
  public static function set_charset($charset) {
      
      if (in_array($charset, HTML::$charsets))
         echo "<meta http-equiv=\"Content-type\" content=text/html; charset=$charset />";
      
      else
          echo '';
  }
  
    
}

?>