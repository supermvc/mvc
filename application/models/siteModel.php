<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class siteModel extends Model_Base {
    
    public static $bbcode_name = 'sfsfsf';
    public static $bbcode_rus_name = 'руснаме';
    public static $bbcode_text = 'bbcode_text';
    public static $bbcode_replacement = 'replacement';
    
    public static function getData() {
        
        DB_MySQL::select('bbcodes');
        
        while ($result = DB_MySQL::fetch())
            Model_Base::$data[] = $result;
        
        return Model_Base::$data;
        
    }
    
    public static function insertData($table) {
        
        if (!DB_MySQL::tableExists($table))
            throw new Exception("Не задана таблица для вставки данных");
        
        $fields = Model_Base::getFieldNames($table);
        
        $insert_data = array();
        $fields_to_insert = array();
        $cur_class = __CLASS__;
        
       foreach ($fields as $value) {
            
            //если существует свойство класса, совпадающее с полем
            if (property_exists('siteModel', $value)) {
                $fields_to_insert[] = $value;
                $insert_data[] = $cur_class::$$value;
            }
        }
        
        if (empty($insert_data))
            throw new Exception("Нет данных для вставки в таблицу");
        
       return DB_MySQL::insert($table, $fields_to_insert, $insert_data);
        
        
        
    }
    
    
    
}
