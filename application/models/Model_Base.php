<?php

class Model_Base extends Base {
    
   
    /**
     * Сюда сохраняются все данные, полученные из таблицы
     * 
     * @var array
     */
    public static $data = array();
    
    /**
     * Таблица, с которой будем работать
     * 
     * @var string
     */
    public static $table = 'bbcodes';
    
    public static function getData() {
        
    }
    
    public static function getFieldNames($table) {
        
        $class = "DB_".Base::$db_layer;
        
        if (!$class::tableExists($table))
            return false;
        
        //сначала делаем минимальную выборку
        $class::select($table);
        
        //получаем список полей
        $fields = $class::fetchFields();
        
        
        
        //здесь будут храниться имена полей
        $field_names = array();
        
                
        //сохраняем имена...
        
        foreach ($fields as $value)
            $field_names[] = $value->name;
            
        
        
        return $field_names;
    }
    
    /**
     * Функция обновляет данные в таблице. Если её не существует, возвращается исключение.
     * 
     * @param string $table Таблица, в которой проводить обновление данных. Если пусто, берется член класса Model_Base
     * @param array $where Массив условий, по которым проводить обновление данных
     */
    public static function updateData($table = '', array $where = array()) {
        
        $class = "DB_".Base::$db_layer;
        
        if (empty($table))
            $table = Model_Base::$table;
        
        if (!$class::tableExists($table))
            throw new Exception("Не могу проводить операцию обновления данных: таблицы $table не существует");
        
        //сначала делаем минимальную выборку
        $class::select($table);
        
             
        
        
        //получаем имена полей
        $field_names = Model_Base::getFieldNames($table);
        
        //данные для обновления
        $update_data = array();
     
        
        //и проходимся по массиву имён
        foreach ($field_names as $value) {
            
            //если существует свойство класса, совпадающее с полем
            if (property_exists('siteModel', $value)) {
                
                $update_data[$value] = siteModel::$$value;
            }
            
        }
        
        //если к этому времени массив $update_data пустой, обновлять нечего
        if (empty($update_data))
            throw new Exception("Нечего обновлять: нет исходных данных");
        
        if (empty($where))
            return $class::update($table, $update_data, array());
        
        else 
            return $class::update($table, $update_data, $where);
        
    }
    
    
    public static function insertData($table) {
        
        $class = "DB_".Base::$db_layer;
        
        if (!$class::tableExists($table))
            throw new Exception("Не задана таблица для вставки данных");
        
        $fields = Model_Base::getFieldNames($table);
        
        $insert_data = array();
        $fields_to_insert = array();
        $cur_class = __CLASS__;
        
       foreach ($fields as $value) {
            
            //если существует свойство класса, совпадающее с полем
            if (property_exists('siteModel', $value)) {
                $fields_to_insert[] = $value;
                $insert_data[] = $cur_class::$$value;
            }
        }
        
        if (empty($insert_data))
            throw new Exception("Нет данных для вставки в таблицу");
        
       return $class::insert($table, $fields_to_insert, $insert_data);
        
        
        
    }
    
}